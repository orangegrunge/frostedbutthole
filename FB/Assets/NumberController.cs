﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using System;

public class NumberController : MonoBehaviour
{
    [Header("Debug")]
    public TextMeshProUGUI text;

    public void Init(Vector2 worldPos, string str, NumbersManager manager)
    {
        text.text = str;
        var screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 localPos = new Vector2();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(manager.GetComponent<RectTransform>(), screenPos, Camera.current, out localPos);
        GetComponent<RectTransform>().anchoredPosition = localPos;
    }

}
