﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ModuleManager : MonoBehaviour, IPointerExitHandler
{
    [Header("Settings")]
    public int initModulesCount;
    public int capacity;
    public float addModuleTime;
    
    public GameObject modulePrefab;
    public StringColorDict colors;

    [Header("Debug")]
    public List<Module> modules;
    public List<Vector3> positions;
    public float timer;
    public bool ready;
    private bool removed;
    public int counter;
    public GameObject back;

    private List<int> typeList;
    //штука для рандома 
    System.Random random;
    private InputController inputController;

    void InitRandom()
    {
        string seed = getRandomSeed();

        random = new System.Random(seed.GetHashCode());
        //random = new System.Random(853006771);
        Debug.Log("GetHashCode: " + seed.GetHashCode());
    }
    string getRandomSeed()
    {
        string seed = "";
        string chars = "QWERTYUIOPASDFGHJKLZXCVBNM";
        for (int i = 0; i < 20; i++)
            seed += chars[UnityEngine.Random.Range(0, chars.Length)];
        return seed;
    }
    public void Start()
    {
        inputController = FindObjectOfType<InputController>();
        //1 - броня
        //2 - фонарь
        //3 - часы
        //4 - турель
        //10 - хилка
        //11 - воздух
        InitRandom();
        typeList = new List<int>() { 1, 2, 3, 11, 10, 2, 3, 1, 2, 3, 10, 4, 2, 3, 1, 2, 4, 1, 2, 10, 1, 11, 2, 10, 4, 1, 2, 3, 1, 4, 11, 3, 2, 10, 1, 2, 3, 11, 1, 10, 2, 3, 1, 2, 10, 1, 2, 3, 10, 1, 2, 3, 1, 11, 2, 3};
        for (int i = 0; i < typeList.Count - 1; i++) {
            int myR = random.Next(1, 3);
            if (myR == 1)
            {
                int c1 = typeList[i + 1];
                typeList[i + 1] = typeList[i];
                typeList[i] = c1;
            }
        }
        for (int i = typeList.Count - 1; i > 0; i--)
        {
            int myR = random.Next(1, 3);
            if (myR == 1)
            {
                int c1 = typeList[i - 1];
                typeList[i - 1] = typeList[i];
                typeList[i] = c1;
            }
        }
        //Debug.Log("typeList");
        //for (int i = 0; i < typeList.Count - 1; i++)
        //{
        //    Debug.Log(typeList[i]);
        //}
        counter = 0;
        modules.ForEach(x => {
            positions.Add(x.GetComponent<Module>().mainTransf.anchoredPosition);
            Destroy(x.gameObject);
        });

        modules = new List<Module>();

        for (int i = 0; i < initModulesCount; i++)
        {
            modules.Add(GenerateModule(i));
            modules[i].moduleView.premade = true;
            modules[i].moduleView.SetIndex(i);
        }
    }

    public Module GenerateModule(int index)
    {
        string[] aliasArray = new string[4];
        //Debug.Log("Index " + index.ToString());
        GameObject moduleObj = Instantiate(modulePrefab, transform);
        RectTransform rectTransform = moduleObj.GetComponent<RectTransform>();
        Module module = moduleObj.GetComponent<Module>();
        module.mainTransf.anchoredPosition = positions[index];
        //module.hintTransf.anchoredPosition += new Vector2(0, positions[index].y);
        module.moduleView.SetIndex(index);

        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1a");
        
        //назначаем тип модуля
        Part part = typeList[counter] != 11 ? (Part)typeList[counter] : Part.CONSUMABLE;

        //назначаем триггер
        Trigger trigger = Trigger.NONE;
        switch (part)
        {
            case Part.HULL://броня
                trigger = Trigger.DAMAGE;
                break;
            case Part.UTILITY://часы
                trigger = Trigger.CLOCK;
                break;
            case Part.WEAPON://фонарь
                if (random.Next(1, 3) == 1)
                {
                    trigger = Trigger.ENEMY;
                }
                else
                {
                    trigger = Trigger.BUBBLE;
                }
                break;
            case Part.AUTOMATION://турель
                trigger = Trigger.NONE;
                break;
            default://расходник
                trigger = Trigger.NONE;
                break;
        }

        //шансы на апгрейд без триггера
        if (part != Part.UTILITY && (
            (counter < 5) ||  //100%
            (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
            (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
            (random.Next(1, 11) < 2 && counter >= 26)))//10%
        {
            trigger = Trigger.NONE;
        }
        //назначаем цену        
        int price = 0;
        //модификатор стадии
        float ratio = counter == 0 ? 1/10: counter / 3;
        int m1 = Convert.ToInt32(Math.Ceiling(ratio)) * 5;
        //модификатор типа
        int m2 = 0;
        switch (part)
        {
            case Part.UTILITY://часы
                m2 = 5;
                break;
            case Part.AUTOMATION://турель
                m2 = 10;
                break;
            default:
                break;
        }
        //модификатор рандома
        int m3 = 0;
        if (counter < 7)
        {
            m3 = (random.Next(1, 4) - 2) * 5;
        }
        else
        {
            if (counter < 20)
            {
                m3 = (random.Next(1, 6) - 3) * 5;
            }
            else
            {
                m3 = (random.Next(1, 8) - 4) * 5;
            }
        }
        //считаем
        price = Mathf.Max(5, m1 + m2 + m3);
        //Debug.Log("Price");
        //Debug.Log(m1);
        //Debug.Log(m2);
        //Debug.Log(m3);

        //назначаем тип турели
        TurretType tt = TurretType.NONE;

        //назначаем статы
        StatIntDict stat = new StatIntDict();

        float ratio2 = counter == 0 ? 1 / 3 : counter / 3;
        int power = Convert.ToInt32(Math.Ceiling(ratio2));
        //power 1-10
        switch (part)
        {
            case Part.HULL://Броня
                aliasArray[2] = "hull";
                if (trigger != Trigger.NONE)
                {
                    int opArmor = -1;
                    //opArmor - возможный оверлап силы
                    if ((counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        opArmor = 0;
                    }
                    if (opArmor == -1 &&
                        (counter < 10) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 11 && counter < 18) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 19 && counter < 26) || //50%
                        (random.Next(1, 11) < 3 && counter >= 27))//20%
                    {
                        opArmor = 2;
                    }
                    if (opArmor == -1)
                    {
                        opArmor = 5;
                    }


                    int typeArmor = 0;                    
                    //решаем что генерим
                    //0- не определились
                    //1- позитив
                    //2- позитивно-негативное
                    //3- негативное
                    if ((counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        typeArmor = 1;
                    }
                    if (typeArmor == 0 &&
                        (counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        typeArmor = 2;
                    }
                    if(typeArmor == 0)
                    {
                        typeArmor = 3;
                    }

                    //добавляем негативный эффект броне
                    if (typeArmor == 2 || typeArmor == 3)
                    {
                        int myR = random.Next(1, 4);
                        int myPowerArmor = random.Next(1, 4);
                        power += myPowerArmor;
                        switch (myR)
                        {
                            case 1:
                                aliasArray[1] = "fragile";
                                int armorNeg = Convert.ToInt32(Math.Ceiling(myPowerArmor * 2.2f));
                                stat.Add(ST.ARMOR_PER_TRIGGER, -armorNeg);
                                break;
                            case 2:
                                int armorNeg2 = Convert.ToInt32(Math.Ceiling(myPowerArmor * 2.2f));
                                aliasArray[1] = "cracked";
                                stat.Add(ST.O2_PER_TRIGGER, -armorNeg2);
                                break;
                            case 3:
                                int armorNeg3 = Convert.ToInt32(Math.Ceiling(myPowerArmor * 1.2f));
                                aliasArray[1] = "greedy";
                                stat.Add(ST.METAL_PER_TRIGGER, -armorNeg3);
                                break;
                            default:
                                break;
                        }

                    }
                    //добавляем позитивный эффект броне
                    if (typeArmor == 2 || typeArmor == 1)
                    {
                        int myPowerArmor = random.Next(1, power + opArmor);
                        power -= myPowerArmor;
                        if (myPowerArmor < 2)
                        {
                            int myR = random.Next(1, 4);
                            switch (myR)
                            {
                                case 1:
                                    if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerArmor);
                                        //module.moduleView.icon.color = colors["sd"];
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerArmor * 3.2f)));
                                    }
                                    break;
                                case 2:
                                    if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerArmor);
                                    }
                                    else
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerArmor);
                                    }
                                    break;                                    
                                case 3:
                                    if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerArmor * 3.2f)));
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerArmor);
                                    }
                                    break;
                                default:
                                    break;
                            }

                        }
                        if (myPowerArmor >= 2)
                        {
                            int myR = random.Next(1, 5);                            
                            switch (myR)
                            {
                                case 1:
                                    if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerArmor);
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerArmor * 3.2f)));
                                    }
                                    break;
                                case 2:
                                    if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerArmor);
                                    }
                                    else
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerArmor);
                                    }
                                    break;
                                case 3:
                                    if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerArmor * 3.2f)));
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerArmor);
                                    }
                                    break;
                                case 4:
                                    aliasArray[3] = "of recharge";
                                    module.moduleView.icon.color = colors["refresh"];
                                    power += myPowerArmor - 1;
                                    stat.Add(ST.CD_SKIPP_PER_TRIGGER, 1);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                }
                if (power <= -3)
                {
                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon2с");
                    aliasArray[0] = "Weak";
                    stat.Add(ST.MAX_ARMOR, 40);
                }
                else
                {
                    if(power == -2)
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon2с");
                        aliasArray[0] = "Weak";
                        stat.Add(ST.MAX_ARMOR, 60);
                    }
                    else
                    {
                        if (power == -1)
                        {
                            module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1с");
                            stat.Add(ST.MAX_ARMOR, 80);
                        }
                        else
                        {
                            if (power == 0)
                            {
                                module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1с");
                                stat.Add(ST.MAX_ARMOR, 100);
                            }
                            else
                            {
                                if (power == 1)
                                {
                                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1с");
                                    stat.Add(ST.MAX_ARMOR, 130);
                                }
                                else
                                {
                                    if (power == 2)
                                    {
                                        price += 5;
                                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon4с");
                                        aliasArray[0] = "Strong";
                                        stat.Add(ST.MAX_ARMOR, 150);
                                    }
                                    else
                                    {
                                        if (power == 3)
                                        {
                                            price += 5;
                                            module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon4с");
                                            aliasArray[0] = "Strong";
                                            stat.Add(ST.MAX_ARMOR, 200);
                                        }
                                        else
                                        {
                                            if (power == 4 || power == 5)
                                            {
                                                price += 10;
                                                module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon3с");
                                                aliasArray[0] = "Diamond";
                                                stat.Add(ST.MAX_ARMOR, 250);
                                            }
                                            else
                                            {
                                                if (power >= 6)
                                                {
                                                    price += 10;
                                                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon3с");
                                                    aliasArray[0] = "Diamond";
                                                    stat.Add(ST.MAX_ARMOR, 300);
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                break;
            case Part.WEAPON://Оружие
                aliasArray[2] = "lantern";
                module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1a");
                if (trigger != Trigger.NONE)
                {
                    int opWeapon = -1;
                    //op - возможный оверлап силы
                    if ((counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        opWeapon = 0;
                    }
                    if (opWeapon == -1 &&
                        (counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        opWeapon = 3;
                    }
                    if (opWeapon == -1)
                    {
                        opWeapon = 6;
                    }


                    int typeWeapon = 0;
                    //решаем что генерим
                    //0- не определились
                    //1- позитив
                    //2- позитивно-негативное
                    //3- негативное
                    if ((counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon4a");
                        typeWeapon = 1;
                    }
                    if (typeWeapon == 0 &&
                        (counter < 5) ||  //100%
                        (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                        (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                        (random.Next(1, 11) < 3 && counter >= 26))//20%
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon3a");
                        typeWeapon = 2;
                    }
                    if (typeWeapon == 0)
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon2a");
                        typeWeapon = 3;
                    }

                    //добавляем негативный эффект оружию
                    if (typeWeapon == 2 || typeWeapon == 3)
                    {
                        int myR = random.Next(1, 4);
                        int myPowerWeapon = random.Next(1, 4);
                        power += myPowerWeapon;
                        switch (myR)
                        {
                            case 1:
                                aliasArray[1] = "fragile";
                                stat.Add(ST.ARMOR_PER_TRIGGER, -myPowerWeapon);
                                break;
                            case 2:
                                aliasArray[1] = "cracked";
                                stat.Add(ST.O2_PER_TRIGGER, -myPowerWeapon);
                                break;
                            case 3:
                                aliasArray[1] = "greedy";
                                stat.Add(ST.METAL_PER_TRIGGER, -myPowerWeapon);
                                break;
                            default:
                                break;
                        }

                    }
                    //добавляем позитивный эффект оружию
                    if (typeWeapon == 2 || typeWeapon == 1)
                    {
                        int myPowerWeapon = random.Next(1, power + opWeapon);
                        power -= myPowerWeapon;
                        if (myPowerWeapon < 5)
                        {
                            int myR = random.Next(1, 4);
                            switch (myR)
                            {
                                case 1:
                                    if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerWeapon);
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerWeapon * 2.2f)));
                                    }
                                    break;
                                case 2:
                                    if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerWeapon);
                                    }
                                    else
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerWeapon);
                                    }
                                    break;
                                case 3:
                                    if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerWeapon * 2.2f)));
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerWeapon);
                                    }
                                    break;
                                default:
                                    break;
                            }

                        }
                        if (myPowerWeapon >= 5)
                        {
                            int myR = random.Next(1, 5);
                            switch (myR)
                            {
                                case 1:
                                    if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerWeapon);
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerWeapon * 2.2f)));
                                    }
                                    break;
                                case 2:
                                    if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerWeapon);
                                    }
                                    else
                                    {
                                        module.moduleView.icon.color = colors["o2"];
                                        aliasArray[3] = "of breath";
                                        stat.Add(ST.O2_PER_TRIGGER, myPowerWeapon);
                                    }
                                    break;
                                case 3:
                                    if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                                    {
                                        aliasArray[3] = "of repair";
                                        module.moduleView.icon.color = colors["hull"];
                                        stat.Add(ST.ARMOR_PER_TRIGGER, Convert.ToInt32(Math.Ceiling(myPowerWeapon * 2.2f)));
                                    }
                                    else
                                    {
                                        aliasArray[3] = "of profit";
                                        module.moduleView.icon.color = colors["cog"];
                                        stat.Add(ST.METAL_PER_TRIGGER, myPowerWeapon);
                                    }
                                    break;
                                case 4:
                                    aliasArray[3] = "of recharge";
                                    module.moduleView.icon.color = colors["refresh"];
                                    power += myPowerWeapon - 4;
                                    stat.Add(ST.CD_SKIPP_PER_TRIGGER, 1);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }

                }
                if (power <= -3)
                {
                    aliasArray[0] = "Slow";
                    stat.Add(ST.CLICK_CD, 8);
                }
                else
                {
                    if (power == -2)
                    {
                        aliasArray[0] = "Slow";
                        stat.Add(ST.CLICK_CD, 7);
                    }
                    else
                    {
                        if (power == -1)
                        {
                            stat.Add(ST.CLICK_CD, 5);
                        }
                        else
                        {
                            if (power == 0)
                            {
                                price += 5;
                                stat.Add(ST.CLICK_CD, 3);
                            }
                            else
                            {
                                if (power == 1)
                                {
                                    price += 5;
                                    stat.Add(ST.CLICK_CD, 2.5f);
                                }
                                else
                                {
                                    if (power == 2)
                                    {
                                        price += 5;
                                        aliasArray[0] = "Fast";
                                        stat.Add(ST.CLICK_CD, 2);
                                    }
                                    else
                                    {
                                        if (power == 3)
                                        {
                                            price += 10;
                                            aliasArray[0] = "Fast";
                                            stat.Add(ST.CLICK_CD, 1.8f);
                                        }
                                        else
                                        {
                                            if (power == 4)
                                            {
                                                price += 15;
                                                aliasArray[0] = "Instant";
                                                stat.Add(ST.CLICK_CD, 1.2f);
                                            }
                                            else
                                            {
                                                if (power >= 5)
                                                {
                                                    price += 15;
                                                    aliasArray[0] = "Instant";
                                                    stat.Add(ST.CLICK_CD, 1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case Part.UTILITY://Часы
                aliasArray[2] = "clock";
                int opClock = -1;
                //op - возможный оверлап силы
                if ((counter < 5) ||  //100%
                    (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                    (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                    (random.Next(1, 11) < 3 && counter >= 26))//20%
                {
                    opClock = 0;
                }
                if (opClock == -1 &&
                    (counter < 5) ||  //100%
                    (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                    (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                    (random.Next(1, 11) < 3 && counter >= 26))//20%
                {
                    opClock = 1;
                }
                if (opClock == -1)
                {
                    opClock = 3;
                }


                int typeClock = 0;
                //решаем что генерим
                //0- не определились
                //1- позитив
                //2- позитивно-негативное
                if ((counter < 5) ||  //100%
                    (random.Next(1, 11) < 9 && counter >= 6 && counter < 12) ||  // 80%
                    (random.Next(1, 11) < 6 && counter >= 13 && counter < 25) || //50%
                    (random.Next(1, 11) < 3 && counter >= 26))//20%
                {
                    if (random.Next(1, 3) == 1)
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1b");
                    }
                    else
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon2b");
                    }                    
                    typeClock = 1;
                }
                if (typeClock == 0)
                {
                    if (random.Next(1, 3) == 1)
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon3b");
                    }
                    else
                    {
                        module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon4b");
                    }                    
                    typeClock = 2;
                }

                //добавляем негативный эффект часам
                if (typeClock == 2)
                {
                    int myR = random.Next(1, 4);
                    int myPowerClock = random.Next(1, 4);
                    power += myPowerClock;
                    switch (myR)
                    {
                        case 1:
                            aliasArray[1] = "fragile";
                            stat.Add(ST.ARMOR_PER_TRIGGER, -myPowerClock);
                            break;
                        case 2:
                            aliasArray[1] = "cracked";
                            stat.Add(ST.O2_PER_TRIGGER, -myPowerClock);
                            break;
                        case 3:
                            aliasArray[1] = "greedy";
                            stat.Add(ST.METAL_PER_TRIGGER, -Convert.ToInt32(Math.Ceiling(myPowerClock * 0.5f)));
                            break;
                        default:
                            break;
                    }

                }
                //добавляем позитивный эффект часам
                int myPower = random.Next(1, Mathf.Max(power + opClock, 2));
                power -= myPower;
                if (myPower < 5)
                {
                    int myR = random.Next(1, 4);
                    switch (myR)
                    {
                        case 1:
                            if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                            {
                                module.moduleView.icon.color = colors["o2"];
                                aliasArray[3] = "of breath";
                                stat.Add(ST.O2_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            else
                            {
                                aliasArray[3] = "of repair";
                                module.moduleView.icon.color = colors["hull"];
                                stat.Add(ST.ARMOR_PER_TRIGGER, Mathf.Max(myPower, 3));
                            }
                            break;
                        case 2:
                            if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                            {
                                aliasArray[3] = "of profit";
                                module.moduleView.icon.color = colors["cog"];
                                stat.Add(ST.METAL_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            else
                            {
                                module.moduleView.icon.color = colors["o2"];
                                aliasArray[3] = "of breath";
                                stat.Add(ST.O2_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            break;
                        case 3:
                            if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                            {
                                aliasArray[3] = "of repair";
                                module.moduleView.icon.color = colors["hull"];
                                stat.Add(ST.ARMOR_PER_TRIGGER, Mathf.Max(myPower, 3));
                            }
                            else
                            {
                                aliasArray[3] = "of profit";
                                module.moduleView.icon.color = colors["cog"];
                                stat.Add(ST.METAL_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            break;
                        default:
                            break;
                    }

                }
                if (myPower >= 5)
                {
                    int myR = random.Next(1, 5);
                    switch (myR)
                    {
                        case 1:
                            if (stat.ContainsKey(ST.ARMOR_PER_TRIGGER))
                            {
                                module.moduleView.icon.color = colors["o2"];
                                aliasArray[3] = "of breath";
                                stat.Add(ST.O2_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            else
                            {
                                aliasArray[3] = "of repair";
                                module.moduleView.icon.color = colors["hull"];
                                stat.Add(ST.ARMOR_PER_TRIGGER, Mathf.Max(myPower, 3));
                            }
                            break;
                        case 2:
                            if (stat.ContainsKey(ST.O2_PER_TRIGGER))
                            {
                                aliasArray[3] = "of profit";
                                module.moduleView.icon.color = colors["cog"];
                                stat.Add(ST.METAL_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            else
                            {
                                module.moduleView.icon.color = colors["o2"];
                                aliasArray[3] = "of breath";
                                stat.Add(ST.O2_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            break;
                        case 3:
                            if (stat.ContainsKey(ST.METAL_PER_TRIGGER))
                            {
                                aliasArray[3] = "of repair";
                                module.moduleView.icon.color = colors["hull"];
                                stat.Add(ST.ARMOR_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            else
                            {
                                aliasArray[3] = "of profit";
                                module.moduleView.icon.color = colors["cog"];
                                stat.Add(ST.METAL_PER_TRIGGER, Mathf.Max(myPower, 2));
                            }
                            break;
                        case 4:
                            power += myPower - 4;
                            aliasArray[3] = "of recharge";
                            module.moduleView.icon.color = colors["refresh"];
                            stat.Add(ST.CD_SKIPP_PER_TRIGGER, 1);
                            break;
                        default:
                            break;
                    }
                }

                if (power <= -3)
                {
                    aliasArray[0] = "Slow";
                    stat.Add(ST.CLOCK_CD, 12);
                }
                else
                {
                    if (power == -2)
                    {
                        aliasArray[0] = "Slow";
                        stat.Add(ST.CLOCK_CD, 9);
                    }
                    else
                    {
                        if (power == -1)
                        {
                            stat.Add(ST.CLOCK_CD, 7);
                        }
                        else
                        {
                            if (power == 0)
                            {
                                stat.Add(ST.CLOCK_CD, 5);
                            }
                            else
                            {
                                if (power == 1)
                                {
                                    stat.Add(ST.CLOCK_CD, 4);
                                }
                                else
                                {
                                    if (power == 2)
                                    {
                                        aliasArray[0] = "Fast";
                                        stat.Add(ST.CLOCK_CD, 2.5f);
                                    }
                                    else
                                    {
                                        if (power == 3)
                                        {
                                            aliasArray[0] = "Fast";
                                            stat.Add(ST.CLOCK_CD, 2);
                                        }
                                        else
                                        {
                                            if (power == 4)
                                            {
                                                aliasArray[0] = "Instant";
                                                stat.Add(ST.CLOCK_CD, 1);
                                            }
                                            else
                                            {
                                                if (power >= 5)
                                                {
                                                    aliasArray[0] = "Instant";
                                                    stat.Add(ST.CLOCK_CD, 0.5f);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                break;
            case Part.AUTOMATION://Турель
                aliasArray[2] = "turret";
                power = power - 5;
                int turretR = random.Next(1, 3);
                if(turretR == 1)
                {
                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon1d");
                    aliasArray[1] = "fish";
                    tt = TurretType.FISH;
                }
                else
                {
                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icon2d");
                    aliasArray[1] = "oxygen";
                    tt = TurretType.BUBBLE;
                }
                if (power <= -3)
                {
                    aliasArray[0] = "Slow";
                    stat.Add(ST.AUTOMATION_CD, 8);
                }
                else
                {
                    if (power == -2)
                    {
                        aliasArray[0] = "Slow";
                        stat.Add(ST.AUTOMATION_CD, 7);
                    }
                    else
                    {
                        if (power == -1)
                        {
                            stat.Add(ST.AUTOMATION_CD, 6);
                        }
                        else
                        {
                            if (power == 0)
                            {
                                stat.Add(ST.AUTOMATION_CD, 5);
                            }
                            else
                            {
                                if (power == 1)
                                {
                                    stat.Add(ST.AUTOMATION_CD, 4);
                                }
                                else
                                {
                                    if (power == 2)
                                    {
                                        aliasArray[0] = "Fast";
                                        stat.Add(ST.AUTOMATION_CD, 3);
                                    }
                                    else
                                    {
                                        if (power == 3)
                                        {
                                            aliasArray[0] = "Fast";
                                            stat.Add(ST.AUTOMATION_CD, 2);
                                        }
                                        else
                                        {
                                            if (power == 4)
                                            {
                                                aliasArray[0] = "Instant";
                                                stat.Add(ST.AUTOMATION_CD, 1.5f);
                                            }
                                            else
                                            {
                                                if (power >= 5)
                                                {
                                                    aliasArray[0] = "Instant";
                                                    stat.Add(ST.AUTOMATION_CD, 1);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                break;
            //расходник
            default:
                if (typeList[counter] == 11)
                {
                    module.moduleView.icon.color = colors["o2"];
                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/icono2");
                    aliasArray[2] = "O2 tank";
                    stat.Add(ST.O2, 30);
                    price = 5;
                }
                else
                {
                    module.moduleView.icon.color = colors["hull"];
                    module.moduleView.icon.sprite = Resources.Load<Sprite>("Icons/iconhull");
                    aliasArray[2] = "Repair kit";
                    stat.Add(ST.ARMOR, 50);
                    price = 10;
                }
                break;
        }
        string finalString = "";
        for (int i = 0; i < 4; i++)
        {
            if (aliasArray[i] != null)
            {
                finalString += aliasArray[i];
                finalString += " ";
            }
        }

        char[] a = finalString.ToCharArray();
        a[0] = char.ToUpper(a[0]);
        string resultAlias = new string(a);

        //Debug.Log(resultAlias);

        string partPrefabPath = "";
        switch (part)
        {
            case Part.AUTOMATION:
                partPrefabPath = "Parts/AUTOMATION 1";
                break;
            case Part.HULL:
                partPrefabPath = "Parts/HULL 1";
                break;
            case Part.UTILITY:
                partPrefabPath = "Parts/UTILITY 1";
                break;
            case Part.WEAPON:
                partPrefabPath = "Parts/WEAPON 1";
                break;
            default:
                partPrefabPath = "";
                break;
        }


        module.Init(part, trigger, price, stat, tt, resultAlias, partPrefabPath);
        counter++;
        return module;
    }

    internal void Move(Module module)
    {
        int index = modules.FindIndex(x => x == module);
        for (int i = 1; i < modules.Count; i++)
        {
            if (modules[i - 1] == module || modules[i-1] == null) {
                modules[i - 1] = modules[i];
                modules[i - 1].moduleView.Move(positions[i-1]);
                modules[i] = null;
            }
        }
        modules = modules.GetRange(0, Math.Max(modules.Count - 1, 0));
    }

    private void Update()
    {
        bool isNotMoving = modules.TrueForAll(x => !x.moduleView.move);

        if (ready && capacity > modules.Count && isNotMoving)
        {
            modules.Add(GenerateModule(modules.Count));
            timer = 0;
            ready = false;
            removed = false;
        }
        else if (ready && !removed)
        {
            modules[0].moduleView.Remove();
            removed = true;
        }
        else
        {
            timer += Time.deltaTime;
            if (timer > addModuleTime) ready = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Unpause();
    }

    public void Unpause()
    {
        inputController.Unpause();
        modules.ForEach(x => x.moduleView.HideHint());
        modules.ForEach(x =>
        {
            if (x.bought)
            {
                x.Destroy();
            }
        });
        back.SetActive(false);
    }
}