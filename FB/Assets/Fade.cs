﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    InputController inputController;
    PlayerController playerController;
    Animator animator;
    LevelStarter levelStarter;

    private void Start()
    {
        inputController = FindObjectOfType<InputController>();
        playerController = FindObjectOfType<PlayerController>();
        levelStarter = FindObjectOfType<LevelStarter>();
        animator = GetComponent<Animator>();

        if (levelStarter.started)
        {
            SetTrigger("Restart");
        }
        else SetTrigger("Start");
    }

    public void SetTrigger(string trigger)
    {
        animator.SetTrigger(trigger);
    }
}
