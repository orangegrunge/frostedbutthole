﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightFader : MonoBehaviour
{
    [Header("Settings")]
    public AnimationCurve intesity;
    public AnimationCurve rChannel;
    public AnimationCurve gChannel;
    public AnimationCurve bChannel;
    public AnimationCurve aChannel;

    [Header("Debug")]
    public InputController inputController;
    public Light2D light2d;

    private void Start()
    {
        inputController = FindObjectOfType<InputController>();
        light2d = GetComponent<Light2D>();
    }

    private void Update()
    {
        light2d.intensity = intesity.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float r = rChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float g = gChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float b = bChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float a = aChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);

        light2d.color = new Color(r, g, b, a);

    }
}
