﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Naut : MonoBehaviour
{
    public bool touch;
    public bool glow;
    public bool end;

    // Update is called once per frame
    void Update()
    {
        if (touch) GetComponent<Animator>().SetTrigger("Final");
        if (glow) FindObjectOfType<PlayerController>().body.SetTrigger("win");
        if (end) FindObjectOfType<Fade>().GetComponent<Animator>().SetTrigger("Win");
        
    }
}
