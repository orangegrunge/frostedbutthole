﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;


public class Rotor : MonoBehaviour
{
    [Header("GD Settings")]
    public bool rotateByUpdate;
    public bool rotateToMouse;
    public bool rotateToTarget;
    public float fullCircleTime;
    public float phase = 0;

    [Header("Debug")]
    public GameObject target;
    public bool lightOn;
    public Light2D light;

    public float multiplier;

    private PlayerController playerController;

    private void Start()
    {
        if (GetComponent<RectTransform>()) phase = 0f;
        playerController = FindObjectOfType<PlayerController>();
        target = null;
        light = GetComponentInChildren<Light2D>();
    }

    void Update()
    {
        if (rotateToTarget)
        {
            SmallLightTargets();
        }


        if (rotateByUpdate) transform.Rotate(new Vector3(0, 0,
            -360f * Time.deltaTime / fullCircleTime
            ));
        else if (rotateToMouse)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
        }
        else if (rotateToTarget && target)
        {
            light.pointLightInnerRadius = Vector3.Distance(target.transform.position, transform.position);
            light.pointLightOuterRadius = Vector3.Distance(target.transform.position, transform.position);
            transform.rotation = Quaternion.LookRotation(Vector3.forward, target.transform.position - transform.position);
        }
        else if (GetComponent<ClockHand>() != null)
        {
            SetPart(playerController.clockCounter / playerController.stats[ST.CLOCK_CD]);
        }
    }

    public void SetPart(float p)
    {
        transform.eulerAngles = new Vector3(0, 0, (-p + phase) * 360f);
    }

    public void Skip()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }

    private void SmallLightTargets()
    {
        if (playerController.turretType == TurretType.BUBBLE)
        {
            BubbleController bubble = playerController.GetComponentInChildren<BubbleDestroyer>().target;
            target = bubble ? bubble.gameObject : null;
            light.enabled = true;
        }
        if (playerController.turretType == TurretType.FISH)
        {
            TorpedoTargetController torpedoTarget = playerController.GetComponentInChildren<TorpedoEmitterController>().target;
            target = torpedoTarget ? torpedoTarget.gameObject : null;
            light.enabled = true;
        }
        if (playerController.turretType == TurretType.NONE)
        {
            light.enabled = false;
        }
    }
}
