﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class RayLight : MonoBehaviour
{
    public Light2D light2D;
    public Vector2 center;
    public float lifeTime;

    public float offset;
    public float scale;
    public float velocity;

    public float counter = 0;
    public AnimationCurve lightCurve;
    public AnimationCurve intencityByDepth;
    internal float counterShift;

    public InputController inputController;

    private void Start()
    {
        inputController = FindObjectOfType<InputController>();
    }

    void Update()
    {
        counter += Time.deltaTime;
        if (counter > lifeTime) Destroy(gameObject);

        transform.position = new Vector2(
            center.x + offset * Mathf.Sin(counterShift + counter * Mathf.Deg2Rad * velocity),
            transform.position.y
            );

        light2D.intensity = Mathf.Clamp(lightCurve.Evaluate(counter / lifeTime), 0f, 1f);
        light2D.intensity *= Mathf.Clamp01(intencityByDepth.Evaluate(inputController.currentDepth / inputController.maxDepth));
    }
}
