﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ModuleView : MonoBehaviour
{
    [Header("Settings")]
    public bool premade;
    public float moveSpeed;

    [Header("Parts")]
    public Image holder;
    public Image icon;
    public Image plate;
    public GameObject hint;
    public Button button;
    public TextMeshProUGUI price;
    public Animator animator;
    public Module module;

    [Header("Debug")]
    public bool isActive = false;
    public PlayerController playerController;
    public bool move;
    private Vector3 targetPos;
   
    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();


        if (premade) animator.SetTrigger("addPremade");
        else animator.SetTrigger("add");
        price.text =  module.price + " <sprite name=\"cog\">";
        CheckPrice();
    }

    private void Update()
    {
        if (move)
        {
            module.mainTransf.anchoredPosition = new Vector2(
                module.mainTransf.anchoredPosition.x,
                Mathf.Min(module.mainTransf.anchoredPosition.y + moveSpeed*Time.deltaTime, targetPos.y)
            );
            /*module.hintTransf.anchoredPosition = new Vector2(
                module.hintTransf.anchoredPosition.x,
                Mathf.Min(module.hintTransf.anchoredPosition.y + moveSpeed * Time.deltaTime, targetPos.y)
            );*/
            if (module.mainTransf.anchoredPosition.y == targetPos.y)
            {
                move = false;
            }
        }
        CheckPrice();
    }

    private void CheckPrice()
    {
        if (module.bought) button.interactable = false;
        else if (playerController.stats[ST.METAL] >= module.price)
        {
            SetActive(true);
        }
        else
        {
            SetActive(false);
        }
    }

    public void SetActive(bool isActive)
    {

        animator.SetBool("active", isActive);
        button.interactable = isActive;
    }

    public void Add()
    {
        animator.SetTrigger("add");
    }

    public void Remove()
    {
        animator.SetTrigger("remove");
        Destroy(gameObject, 0.5f);
    }

    public void ShowHint()
    {
        hint.gameObject.SetActive(true);
    }

    public void HideHint()
    {
        hint.gameObject.SetActive(false);
    }

    public void Buy()
    {
        animator.SetTrigger("buy");
    }

    public void Move(Vector3 target)
    {
        move = true;
        targetPos = target; 
    }

    public void SetIndex(int index)
    {
        animator.SetInteger("index",index);
    }
}
