﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathByTarget : MonoBehaviour
{
    public float x;

    private void Update()
    {
        if (Mathf.Abs(x) <= Mathf.Abs(transform.position.x)) Destroy(gameObject);
    }
}
