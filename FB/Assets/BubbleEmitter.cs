﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleEmitter : MonoBehaviour
{
    [Header("GD Settings")]
    [Range(0f, 1f)]
    public float spawnSidesPart;
    public GameObject BubblePrefub;
    public BubbleTiersFloatDict tiersMinDepth;
    public BubbleTiersFloatDict tiersMaxDepth;
    public BubbleTiersFloatDict tiersDelayMedian;
    public BubbleTiersFloatDict tiersDelayDispersion;
    public BubbleTiersCurveDict spawnSpeedCurves;

    [Header("Debug")]
    public float depth;
    public bool SmallBubbleCD;
    public bool NormalBubbleCD;
    public bool BigBubbleCD;

    private BoxCollider2D SpawnCollider;
    private InputController inputController;

    void Start()
    {
        SpawnCollider = GetComponent<BoxCollider2D>();
        SmallBubbleCD = false;
        NormalBubbleCD = false;
        BigBubbleCD = false;
        inputController = FindObjectOfType<InputController>();
    }

    private void Update()
    {
        depth = inputController.currentDepth;
        SmallBubbleCD = CheckCoroutine(BubbleTier.SMALL, "SpawnSmallBubble", SmallBubbleCD);
        NormalBubbleCD = CheckCoroutine(BubbleTier.NORMAL, "SpawnNormalBubble", NormalBubbleCD);
        BigBubbleCD = CheckCoroutine(BubbleTier.BIG, "SpawnBigBubble", BigBubbleCD);
    }

    IEnumerator SpawnSmallBubble()
    {
        yield return SpawnBubble(BubbleTier.SMALL);
        StartCoroutine("SpawnSmallBubble");
    }

    IEnumerator SpawnNormalBubble()
    {
        yield return SpawnBubble(BubbleTier.NORMAL);
        StartCoroutine("SpawnNormalBubble");
    }

    IEnumerator SpawnBigBubble()
    {
        yield return SpawnBubble(BubbleTier.BIG);
        StartCoroutine("SpawnBigBubble");
    }

    private WaitForSeconds SpawnBubble(BubbleTier tier)
    {
        float spawnX = SpawnCollider.bounds.center.x + GetSpawnX(SpawnCollider.bounds.max.x - SpawnCollider.bounds.min.x);
        float spawnY = Random.Range(SpawnCollider.bounds.min.y, SpawnCollider.bounds.max.y);

        GameObject bubbleObj = Instantiate(BubblePrefub, new Vector3(spawnX, spawnY, 0), Quaternion.identity);
        BubbleController bubble = bubbleObj.GetComponent<BubbleController>();
        bubble.tier = tier;


        float median = tiersDelayMedian[tier];
        float dispersion = Mathf.Min(Mathf.Abs(tiersDelayDispersion[tier]), tiersDelayMedian[tier]);

        float curveScale = spawnSpeedCurves[tier].Evaluate(depth / (tiersMaxDepth[tier] - tiersMinDepth[tier]));

        median *= curveScale;
        dispersion *= curveScale;

        return new WaitForSeconds(
            Random.Range(
                median - dispersion,
                median + dispersion
                )
            );
    }

    private bool CheckGeneratingCondision(BubbleTier tier)
    {
        return depth >= tiersMinDepth[tier] && depth <= tiersMaxDepth[tier];
    }

    private bool CheckCoroutine(BubbleTier tier, string methodName, bool processIndicator)
    {
        if (processIndicator != CheckGeneratingCondision(tier))
        {
            if (CheckGeneratingCondision(tier))
            {
                StartCoroutine(methodName);
                return true;
            }
            else
            {
                StopCoroutine(methodName);
                return false;
            }
        }

        return processIndicator;
    }

    private float GetSpawnX(float width)
    {
        float maxRange = width / 2;
        float minRange = maxRange * (1 - spawnSidesPart);
        float rnd = Random.Range(-1f, 1f);
        float spawnX = Random.Range(minRange, maxRange);
        return (rnd > 0 ? spawnX : spawnX * (-1));
    }
}
