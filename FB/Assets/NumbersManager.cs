﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumbersManager : MonoBehaviour
{
    public GameObject numberPrefab;

    public void PopNumber(Vector2 pos, string text)
    {
        var number = Instantiate(numberPrefab, transform);
        number.GetComponent<NumberController>().Init(pos, text, this);
    }

}
