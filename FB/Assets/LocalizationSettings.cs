﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationSettings : MonoBehaviour
{
    public PartStringDict parts;
    public TurretTypeStringDict turretType;
    public StatStringDict effects;
    public StatStringDict parameters;
    public TriggerStringDict triggers;

    public List<ST> inverted;

    public bool IsInverted(ST st)
    {
        return inverted.FindIndex(x => x == st) != -1;
    }

    public string GetPrefix(float value)
    {
        return value > 0 ? "+" : "";
    }
}
