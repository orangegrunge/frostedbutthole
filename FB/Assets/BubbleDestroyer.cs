﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class BubbleDestroyer : MonoBehaviour
{
    [Header("GD Settings")]
    public float lightStrikeIntensivity;
    public float lightDefaultIntensivity;
    public float findTargetRadius;

    [Header("Debug")]
    public BubbleController target;
    public bool automatic;
    public bool isAutoCD;
    public List<BubbleController> targets = new List<BubbleController>();

    private PlayerController player;
    private Light2D light;
    private AudioSource audio;

    private float lastCD;

    private void Start()
    {
        player = GetComponentInParent<PlayerController>();
        target = null;
        lastCD = player.stats[ST.AUTOMATION_CD];
        light = FindTurretLight();

        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        automatic = player.turretType == TurretType.BUBBLE;
        
        if (automatic)
        {
            if (!light)
            {
                light = FindTurretLight();
            }

            if (!target)
            {
                target = FindTarget();
            }

            if (!isAutoCD && target)
            {
                DestroyBubble();
                StartCoroutine("AutoCD");
            }
        }

        if (!automatic || lastCD != player.stats[ST.AUTOMATION_CD])
        {
            lastCD = player.stats[ST.AUTOMATION_CD];
            StopCoroutine("AutoCD");
            isAutoCD = false;
        }
    }

    private void DestroyBubble()
    {
        StartCoroutine("LightStrike");
    }

    private BubbleController FindTarget()
    {
        float maxDist = findTargetRadius;
        BubbleController resultTarget = null;
        foreach (BubbleController target in targets)
        {
            if (target.transform.position.y < transform.position.y)
            {
                float dist = Vector3.Distance(transform.position, target.transform.position);
                if (dist < maxDist)
                {
                    maxDist = dist;
                    resultTarget = target;
                }
            }
        }

        return resultTarget;
    }

    public void RegisterTarget(BubbleController target)
    {
        targets.Add(target);
    }

    public void UnregisterTarget(BubbleController target)
    {
        targets.Remove(target);
        targets = targets.FindAll(x => x != null);
    }

    IEnumerator AutoCD()
    {
        isAutoCD = true;
        yield return new WaitForSeconds(player.stats[ST.AUTOMATION_CD]);
        isAutoCD = false;
    }

    IEnumerator LightStrike()
    {
        light.intensity = lightStrikeIntensivity;
        audio.Play();
        yield return new WaitForSeconds(0.3f);
        target.ClickReaction();
        light.intensity = lightDefaultIntensivity;
    }

    private Light2D FindTurretLight()
    {
        Light2D result = null;
        foreach (Rotor rotor in FindObjectsOfType<Rotor>())
        {
            if (rotor.rotateToTarget)
            {
                result = rotor.light;
            }
        }
        return result;
    }
}
