﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatIntDict : SerializableDictionary<ST, float>
{
}

[System.Serializable]
public class StringColorDict : SerializableDictionary<string, Color>
{
}

[System.Serializable]
public class PartStringDict : SerializableDictionary<Part, string>
{
}

[System.Serializable]
public class StatStringDict : SerializableDictionary<ST, string>
{
}

[System.Serializable]
public class TriggerStringDict : SerializableDictionary<Trigger, string>
{
}

[System.Serializable]
public class TurretTypeStringDict : SerializableDictionary<TurretType, string>
{
}

[System.Serializable]
public class BubbleTiersFloatDict : SerializableDictionary<BubbleTier, float>
{
}

[System.Serializable]
public class BubbleTiersCurveDict : SerializableDictionary<BubbleTier, AnimationCurve>
{
}

[System.Serializable]
public class EnemyTiersFloatDict : SerializableDictionary<EnemyTier, float>
{
}

[System.Serializable]
public class EnemyTiersCurveDict : SerializableDictionary<EnemyTier, AnimationCurve>
{
}

[System.Serializable]
public class EnemyTiersPrefubDict : SerializableDictionary<EnemyTier, GameObject>
{
}