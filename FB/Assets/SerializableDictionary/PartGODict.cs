﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PartGODict : SerializableDictionary<Part, GameObject>
{
}
