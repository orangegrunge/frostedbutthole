﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatView : MonoBehaviour
{
    [Header("Settings")]
    public PartGODict parts;
    public bool dead;


    private void Update()
    {
        if (dead)
        {
            FindObjectOfType<Fade>().SetTrigger("Die");
            
        }
    }

    internal void ReplaceModule(Module module)
    {
        GameObject part = parts[module.part];
        Vector3 pos = part.transform.position;
        Transform parent = part.transform.parent;

        //GameObject newPart = Instantiate(module.partPrefab);
        //newPart.transform.position = pos;
        //newPart.transform.parent = parent;

        //part.GetComponent<PartController>().Remove();

        //parts[module.part] = newPart;
        //newPart.GetComponent<PartController>().Add();
    }
}
