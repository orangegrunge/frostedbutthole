﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatsUI : MonoBehaviour
{
    public Slider hullBar;
    public Slider o2Bar;
    public TextMeshProUGUI hullText;
    public TextMeshProUGUI o2Text;
    public TextMeshProUGUI meText;
    public Image clockFace;
    public Image clockCase;
    public Image clockHand;
    public Animator clockAnimator;
    
    [Header("Debug")]
    public PlayerController playerController;
    public bool clockShown;
    public bool clockReplacing;
    public Sprite newFace;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();


    }

    private void Update()
    {
        DrawBars();
        DrawStats();

        ClockReplaceDelayed();
    }

    private void ClockReplaceDelayed()
    {
        if (clockReplacing && clockCase.GetComponent<RectTransform>().eulerAngles.z >= 80)
        {
            clockReplacing = false;
            clockFace.sprite = newFace;
            clockAnimator.SetTrigger("show");
        }
    }

    private void DrawStats()
    {
        hullText.text = string.Format("{0}/{1} <sprite name=\"hp\">",
            Mathf.Ceil(playerController.stats[ST.ARMOR]),
            Mathf.Ceil(playerController.stats[ST.MAX_ARMOR])
        );
        o2Text.text = string.Format("{0} <sprite name=\"o\">",
            Mathf.Ceil(playerController.stats[ST.O2])
        );
        meText.text = string.Format("{0} <sprite name=\"cog\">", Mathf.Ceil(playerController.stats[ST.METAL]));
    }

    private void DrawBars()
    {
        hullBar.value = playerController.stats[ST.ARMOR] / playerController.stats[ST.MAX_ARMOR];
        o2Bar.value = playerController.stats[ST.O2] / playerController.stats[ST.MAX_O2];
    }

    [ContextMenu("Replace")]
    public void ReplaceClocks()
    {
        if (clockShown)
        {
            clockAnimator.SetTrigger("hide");
        }
        else clockShown = true;
        clockReplacing = true;
    }

}
