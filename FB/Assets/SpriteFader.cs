﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SpriteFader : MonoBehaviour
{
    [Header("Settings")]
    public bool recursive;
    public AnimationCurve rChannel;
    public AnimationCurve gChannel;
    public AnimationCurve bChannel;
    public AnimationCurve aChannel;
    
    [Header("Debug")]
    public InputController inputController;
    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();

    private void Start()
    {
        inputController = FindObjectOfType<InputController>();
        
        if (recursive)
        {
            sprites = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
            sprites = sprites.FindAll(x => x.GetComponent<SpriteFader>() == null);
        }
        sprites.Add(GetComponent<SpriteRenderer>());
    }

    private void Update()
    {
        float r = rChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float g = gChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float b = bChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        float a = aChannel.Evaluate(inputController.currentDepth / inputController.maxDepth);
        Color color = new Color(r, g, b, a);

        sprites.ForEach(x =>
        {
            x.color = color;
        });
        

    }
}
