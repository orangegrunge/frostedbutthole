﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public void Restart()
    {
        FindObjectOfType<LevelStarter>().started = true;
        SceneManager.LoadScene(0);
    }
}
