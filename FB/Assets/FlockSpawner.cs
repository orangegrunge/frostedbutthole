﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FlockSpawner : MonoBehaviour
{
    public int minSize;
    public int maxSize;
    public GameObject leader;
    public GameObject flocking;
    public AnimationCurve spawnRate;
    public float minVelocity;
    public float maxVelocity;
    public float maxScale;
    public float minScale;
    public float minSpawnCD;
    public float maxSpawnCD;

    public float leftBound;
    public float rightBound;

    [Header("Debug")]
    public float timer;
    public float second;
    public InputController inputController;
    public int flockCounter;

    private void Start()
    {
        inputController = FindObjectOfType<InputController>();
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > minSpawnCD)
        {
            if ((int)timer > second)
            {
                second = (int)timer;
                if (UnityEngine.Random.value < spawnRate.Evaluate(inputController.currentDepth/inputController.maxDepth))
                {
                    SpawnFlock();
                    timer = 0;
                } 
            }
        }
        else if (timer > maxSpawnCD)
        {
            SpawnFlock();
            timer = 0;
        }

        
    }

    private void SpawnFlock()
    {
        flockCounter++;

        float spawnY = UnityEngine.Random.Range(-5f, 5f);
        float spawnX = UnityEngine.Random.value > 0.5f ? -12f : 12f;

        Vector2 pos = new Vector2(spawnX, spawnY);

        int size = Random.Range(minSize, maxSize);

        for (int i = 0; i < size; i++)
        {
            if (i == 0 && leader)
            {
                SpawnSingle(leader, pos);
            }
            else SpawnSingle(flocking, pos);
        }


    }

    private void SpawnSingle(GameObject flocking, Vector2 pos)
    {
        GameObject fish = Instantiate(flocking);
        fish.transform.position = Random.insideUnitCircle * 2 + pos;
        fish.transform.localScale = Random.Range(minScale, maxScale) * flocking.transform.localScale;

        fish.transform.localScale = new Vector3(
            (fish.transform.position.x < 0) 
                ? fish.transform.localScale.x 
                : -fish.transform.localScale.x,
            fish.transform.localScale.y,
            fish.transform.localScale.z
            );

        var beh = fish.GetComponent<Steer2D.Arrive>();
        beh.TargetPoint = new Vector2(-pos.x, pos.y + 20);

        var fl = fish.GetComponent<Steer2D.Flock>();
        fl.flockTag = flockCounter;

        var dBT = fish.GetComponent<DeathByTarget>();
        dBT.x = -pos.x;

        var steer = flocking.GetComponent<Steer2D.SteeringAgent>();
        steer.MaxVelocity = Random.Range(minVelocity, maxVelocity);
    }
}
