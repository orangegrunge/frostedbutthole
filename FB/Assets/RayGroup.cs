﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayGroup : MonoBehaviour
{

    public List<GameObject> prefabs;
    public Transform leftAndYSpawnBodrer;
    public Transform rightSpawnBodrer;
    public float minLifeTime;
    public float maxLifeTime;
    public float minOffset;
    public float maxOffset;
    public float minScale;
    public float maxScale;
    public float minVelocity;
    public float maxVelocity;
    public int spriteToUseCount;
    public AnimationCurve spawnRate;
    public float minSpawnCD;
    

    [Header("Debug")]
    public float timer;
    public float second;
    public InputController inputController;

    private void Start()
    {
        
        inputController = FindObjectOfType<InputController>();
        Spawn();
        Spawn(); 
        Spawn();
        Spawn(); 
        Spawn();
        Spawn();
        Spawn();
        Spawn();
        Spawn();
        Spawn();
    }

    private void Update()
    {

        timer += Time.deltaTime;
        if (timer > minSpawnCD)
        {
            if ((int)timer > second)
            {
                second = (int)timer;
                if (UnityEngine.Random.value < spawnRate.Evaluate(inputController.currentDepth / inputController.maxDepth))
                {
                    Spawn();
                    timer = 0;
                }
            }
        }
    }

    public void Spawn()
    {
        int flip = Random.value > 0.5f ? 1 : -1;
        flip = 1;


        List<GameObject> spritesToSpawn = new List<GameObject>();
        if (spriteToUseCount <= 0 || spriteToUseCount > prefabs.Count)
        {
            spritesToSpawn = prefabs;
        }
        else
        {
            var temp = prefabs.GetRange(0, prefabs.Count);
            for (int i = 0; i<spriteToUseCount; i++)
            {
                int index = Random.Range(0, temp.Count);
                spritesToSpawn.Add(temp[index]);
                temp = temp.FindAll(x => x != temp[index]);
            }
        }

        Vector2 center = new Vector2(
                Random.Range(leftAndYSpawnBodrer.position.x, rightSpawnBodrer.position.x),
                leftAndYSpawnBodrer.position.y
                );

        foreach (GameObject spr in spritesToSpawn)
        {
            GameObject rayObj = Instantiate(spr);
            RayLight rayLight = rayObj.GetComponent<RayLight>();

            rayLight.counterShift = Random.Range(0f, 360f);
            rayLight.center = center;
            rayLight.offset = Random.Range(minOffset, maxOffset);
            rayLight.velocity = Random.Range(minVelocity, maxVelocity);
            rayLight.lifeTime = Random.Range(minLifeTime, maxLifeTime);

            rayObj.transform.localScale = new Vector2(
                Random.Range(minScale, maxScale) * rayObj.transform.localScale.x * flip,
                rayObj.transform.localScale.y
                );
            

        }
        
    }
}
