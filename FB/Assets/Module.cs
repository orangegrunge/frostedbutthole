﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum TurretType
{
    NONE,
    FISH,
    BUBBLE
}

public struct DescStruct
{
    public ST param;
    public ST posEffect;
    public ST negEffect;

    public DescStruct(bool formal)
    {
        param = ST.NONE;
        posEffect = ST.NONE;
        negEffect = ST.NONE;
    }
}

public class Module : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Settings")]
    public Part part;
    public GameObject partPrefab;
    public StatIntDict stats;
    public int price;
    public Trigger trigger;
    public bool fake;
    public Sprite clockFace;
    public AudioClip init;
    public AudioClip apply;
    public GameObject soundObject;
    public float timeBeforeDeath;

    [Header("Debug")]
    public PlayerController player;
    private InputController inputController;
    private ModuleManager moduleManager;
    private BoatHintsController boatHintsController;
    public ModuleView moduleView;
    public RectTransform mainTransf;
    public RectTransform hintTransf;
    public bool bought;
    public string description;
    public string alias;
    public SoundController sc;

    public TurretType tType;

    public AudioSource audio;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        inputController = FindObjectOfType<InputController>();
        moduleManager = FindObjectOfType<ModuleManager>();
        boatHintsController = FindObjectOfType<BoatHintsController>();

        audio = GetComponent<AudioSource>();
        audio.clip = init;
        if (inputController.currentDepth >= 1f)
        {
            audio.Play();
        }

        sc = Instantiate(soundObject, new Vector3(0, 0, 0), Quaternion.identity).GetComponent<SoundController>();
        sc.clip = apply;
        sc.timeBeforeDeath = timeBeforeDeath;
        sc.volume = audio.volume;
    }

    public void Init(Part p, Trigger t, int pr, StatIntDict st, TurretType turret, string al, string partPrefabPath)
    {
        alias = al;
        part = p;
        trigger = t;
        price = pr;
        stats = st;
        tType = turret;
        partPrefab = partPrefabPath != "" ? Resources.Load<GameObject>(partPrefabPath) : null;

        SetDescription();
    }

    void Update()
    {

    }

    [ContextMenu("APPLY")]
    public void Apply()
    {

        player.AddModule(this);
        moduleView.Buy();
        if (part == Part.UTILITY)
        {
            var statsUI = FindObjectOfType<StatsUI>();
            statsUI.newFace = clockFace;
            statsUI.ReplaceClocks();
        }
        bought = true;
    }

    public void Destroy()
    {
        sc.Death();
        Destroy(gameObject, 1f);
    }


    private void OnDestroy()
    {
        if (fake) return;
        var mm = FindObjectOfType<ModuleManager>();
        if (mm) mm.Move(this);

    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        inputController.Pause();
        moduleManager.back.SetActive(true);
        boatHintsController.ShowHint(part);
        moduleView.ShowHint();
        var views = FindObjectsOfType<ModuleView>();
        foreach (ModuleView view in views)
        {
            if (view != moduleView) view.HideHint();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }


    public void SetDescription()
    {
        var n = System.Environment.NewLine;
        var locale = FindObjectOfType<LocalizationSettings>();
        DescStruct desc = new DescStruct(false);

        description = string.Format("<b><u>{0}</u></b>" + n, alias);


        foreach (ST st in locale.effects.Keys)
        {
            if (!stats.ContainsKey(st)) continue;
            if (stats[st] > 0)
            {
                if (!locale.IsInverted(st)) desc.posEffect = st;
                else desc.negEffect = st;
            }
            else
            {
                if (!locale.IsInverted(st)) desc.negEffect = st;
                else desc.posEffect = st;
            }
        }

        foreach (ST st in locale.parameters.Keys)
        {
            if (!stats.ContainsKey(st)) continue;
            desc.param = st;
        }

        description += string.Format("<i>{0}{1}</i>" + n,
            locale.parts[part],
            locale.turretType[tType]
            );

        if (part != Part.UTILITY)
        {

            if (desc.param != ST.NONE)
            {
                if (part == Part.WEAPON)
                {
                    description += string.Format("<b>{0}</b> {1}{2}" + n,
                        stats[desc.param],
                        " sec. ",
                        locale.parameters[desc.param]
    );
                }
                else
                {
                    description += string.Format("<b>{0}</b> {1}" + n,
    stats[desc.param],
    locale.parameters[desc.param]
    );
                }


            }
        }

        //Debug.Log(trigger);
        if (trigger != Trigger.NONE && trigger != Trigger.CLOCK)
        {
            description = description + string.Format("{0}: ", locale.triggers[trigger]);
        }


        if (desc.posEffect != ST.NONE)
        {
            if (desc.posEffect != ST.CD_SKIPP_PER_TRIGGER)
            {
                description += string.Format("<color=\"green\">{0}{1} {2}</color>",
                locale.GetPrefix(stats[desc.posEffect]),
                stats[desc.posEffect],
                locale.effects[desc.posEffect]
                );
            }
            else
            {
                description += string.Format("<color=\"green\">{0}</color>",
                locale.effects[desc.posEffect]
                );
            }

            if (desc.negEffect != ST.NONE) description = description + ", ";
        }
        if (desc.negEffect != ST.NONE)
        {
            description += string.Format("<color=\"red\">{0}{1} {2}</color>",
                locale.GetPrefix(stats[desc.negEffect]),
                stats[desc.negEffect],
                locale.effects[desc.negEffect]
                );
        }

        if (part == Part.UTILITY)
        {
            description += n + string.Format("every <b>{0}</b> seconds",
                stats[desc.param]
                );
        }

        moduleView.hint.GetComponent<Hint>().desc.text = description;
    }
}