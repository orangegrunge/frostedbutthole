﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ST
{
    O2,
    MAX_O2,
    ARMOR,
    MAX_ARMOR,
    METAL,
    MAX_METAL,
    O2_PER_MIN_DEP,

    CLICK_CD,
    CLOCK_CD,
    AUTOMATION_CD,

    O2_PER_TRIGGER,
    METAL_PER_TRIGGER,
    ARMOR_PER_TRIGGER,
    CD_SKIPP_PER_TRIGGER,

    NONE = 10000
}

public enum Part
{
    WEAPON = 2,
    HULL = 1,
    UTILITY = 3,
    AUTOMATION = 4, // ????
    CONSUMABLE = 10,
}

public enum Trigger
{
    NONE,
    BUBBLE,
    ENEMY,
    DAMAGE,
    CLOCK
}

public class PlayerController : MonoBehaviour
{
    [Header("Settings")]
    public StatIntDict stats;
    public AudioClip tick;
    [Header("Debug")]
    public BoatView boatView;
    public LocalizationSettings locale;
    public NumbersManager numbersManager;
    public BoatHintsController hintsController;

    public InputController inputController { get; private set; }

    public Dictionary<Part, StatIntDict> modules = new Dictionary<Part, StatIntDict>();
    public Dictionary<Part, Trigger> moduleTriggers = new Dictionary<Part, Trigger>();
    public StatIntDict debugCurrentStats = new StatIntDict();
    public float clockCounter;
    public Transform clockNumberPopPosition;

    public TurretType turretType;

    private TorpedoEmitterController torpedoEmitter;
    public Animator body;
    public Animator fins;

    private AudioSource audio;
    private bool win;

    public void DepleteOxygen() {
        
        if (!win) stats[ST.O2] -= stats[ST.O2_PER_MIN_DEP] / 60 * Time.deltaTime;

        foreach (StatIntDict st in modules.Values)
        {
            if (st.ContainsKey(ST.O2_PER_MIN_DEP))
            {
                stats[ST.O2] -= st[ST.O2_PER_MIN_DEP] / 60 * Time.deltaTime;
            }
        }
        ClampStats();
        if (stats[ST.O2] <= 0)
        {
            body.SetTrigger("Death");
            fins.SetTrigger("die");
            FindObjectOfType<InputController>().Die();
        } 

    }

    public void ClampStats()
    {
        stats[ST.O2] = Mathf.Clamp(stats[ST.O2], 0, stats[ST.MAX_O2]);
        stats[ST.ARMOR] = Mathf.Clamp(stats[ST.ARMOR], 0, stats[ST.MAX_ARMOR]);
        stats[ST.METAL] = Mathf.Max(stats[ST.METAL], 0);
    }

    internal void AddModule(Module module)
    {
        stats[ST.METAL] -= module.price;

        if (module.part == Part.UTILITY) clockCounter = 0;

        if (module.part == Part.CONSUMABLE)
        {
            foreach (ST st in (ST[])Enum.GetValues(typeof(ST)))
            {
                if (module.stats.ContainsKey(st))
                {
                    if (stats.ContainsKey(st)) stats[st] += module.stats[st];
                    else stats.Add(st, module.stats[st]);
                }
            }
        }
        else
        {
            boatView.ReplaceModule(module);
            hintsController.SetDesc(module.part, module.description);
            if (modules.ContainsKey(module.part)) RemoveSats(modules[module.part]);
            AddStats(module.stats);
            modules[module.part] = module.stats;
            moduleTriggers[module.part] = module.trigger;
        }
        ClampStats();

        if (module.part == Part.AUTOMATION)
        {
            turretType = module.tType;
        }
    }


    public void RemoveSats(StatIntDict st)
    {
        foreach (ST s in (ST[])Enum.GetValues(typeof(ST)))
        {
            if (s == ST.MAX_ARMOR || s == ST.ARMOR) continue;
            if (st.ContainsKey(s))
            {
                if (stats.ContainsKey(s)) stats[s] -= st[s];
            }

        }
    }

    public void AddStats(StatIntDict st)
    {
        foreach (ST s in (ST[])Enum.GetValues(typeof(ST)))
        {
            if (st.ContainsKey(s))
            {
                if (stats.ContainsKey(s)) {
                    if (locale.parameters.ContainsKey(s)) {
                        if (s == ST.MAX_ARMOR)
                        {
                            stats[ST.ARMOR] += (st[s] > stats[s]) ? (st[s] - stats[s]) : 0;
                        }
                        stats[s] = st[s];
                        
                    }
                    else stats[s] = stats[s] + st[s];
                }
                else stats.Add(s, st[s]);
            }
        }
    }

    public void Start()
    {
        boatView = FindObjectOfType<BoatView>();
        locale = FindObjectOfType<LocalizationSettings>();
        numbersManager = FindObjectOfType<NumbersManager>();
        hintsController = FindObjectOfType<BoatHintsController>();
        inputController = FindObjectOfType<InputController>();
        turretType = TurretType.NONE;

        audio = GetComponent<AudioSource>();
        audio.clip = tick;
    }

    private void Update()
    {
        DepleteOxygen();
        CLockCounter();
        if (stats[ST.ARMOR] <= 0) stats[ST.O2_PER_MIN_DEP] = 800f;
        UpdateCurrentStats();
    }

    private void UpdateCurrentStats()
    {
        foreach (ST st in (ST[])Enum.GetValues(typeof(ST)))
        {
            debugCurrentStats[st] = stats.ContainsKey(st) ? stats[st] : 0;
            foreach (StatIntDict stm in modules.Values)
            {
                if (stm.ContainsKey(st))
                {
                    debugCurrentStats[st] += stm[st];
                }
            }
        }
    }

    public void CallTrigger(Trigger t, float value, Vector3 position, float metal)
    {
        if (win) return;

        StatIntDict statIncrement = new StatIntDict();
        statIncrement.Add(ST.METAL, 0);
        statIncrement.Add(ST.O2, 0);
        statIncrement.Add(ST.ARMOR, 0);
        statIncrement.Add(ST.CD_SKIPP_PER_TRIGGER, 0);

        foreach (Part part in moduleTriggers.Keys)
        {
            if (moduleTriggers[part] == t)
            {
                if (modules[part].ContainsKey(ST.METAL_PER_TRIGGER))
                {
                    stats[ST.METAL] += modules[part][ST.METAL_PER_TRIGGER];
                    statIncrement[ST.METAL] += modules[part][ST.METAL_PER_TRIGGER];
                }
                if (modules[part].ContainsKey(ST.O2_PER_TRIGGER)) {
                    stats[ST.O2] += modules[part][ST.O2_PER_TRIGGER];
                    statIncrement[ST.O2] += modules[part][ST.O2_PER_TRIGGER];
                }
                if (modules[part].ContainsKey(ST.ARMOR_PER_TRIGGER))
                {
                    stats[ST.ARMOR] += modules[part][ST.ARMOR_PER_TRIGGER];
                    statIncrement[ST.ARMOR] += modules[part][ST.ARMOR_PER_TRIGGER];

                }
                if (modules[part].ContainsKey(ST.CD_SKIPP_PER_TRIGGER))
                {
                    statIncrement[ST.CD_SKIPP_PER_TRIGGER] = 1;
                    StartCoroutine(SkipCD());
                }
            }
        }

        if (t == Trigger.BUBBLE)
        {
            stats[ST.O2] += value;
            statIncrement[ST.O2] += value;
        }
        if (t == Trigger.ENEMY) {
            stats[ST.METAL] += value;
            statIncrement[ST.METAL] += value;
        }
        if (t == Trigger.DAMAGE) {
            stats[ST.ARMOR] += -value;
            statIncrement[ST.ARMOR] += -value;
        }

        stats[ST.METAL] += metal;
        statIncrement[ST.METAL] += metal;

        ClampStats();

        string str = "";
        foreach (ST s in statIncrement.Keys)
        {
            if (statIncrement[s] != 0)
            {
                if (str != "") str += Environment.NewLine;
                if (s == ST.CD_SKIPP_PER_TRIGGER) str += "<size=14>RELOADED!</size>";
                else str += locale.GetPrefix(statIncrement[s]) + statIncrement[s] + locale.effects[s];
            }
        }
        numbersManager.PopNumber(position, str);
    }

    public void CLockCounter()
    {
        clockCounter = clockCounter + Time.deltaTime;
        if (clockCounter > stats[ST.CLOCK_CD])
        {
            audio.Play();
            CallTrigger(Trigger.CLOCK, 0, clockNumberPopPosition.position, 0);
            clockCounter = 0;
        }
        
    }

    public void Win()
    {
        win = true;
    }

    IEnumerator SkipCD()
    {
        yield return new WaitForEndOfFrame();
        inputController.CurrentDelay = stats[ST.CLICK_CD];
    }
}
