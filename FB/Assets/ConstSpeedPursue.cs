﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steer2D;

public class ConstSpeedPursue : SteeringBehaviour
{
    public SteeringAgent TargetAgent;

    public override Vector2 GetVelocity()
    {
        float t = 1f / TargetAgent.MaxVelocity;
        Vector2 targetPoint = (Vector2)TargetAgent.transform.position + TargetAgent.CurrentVelocity * t;

        return ((targetPoint - (Vector2)transform.position).normalized * agent.MaxVelocity) - agent.CurrentVelocity;
    }
}
