﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioClip clip;
    public float timeBeforeDeath;
    public float volume;

    private AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.clip = clip;
        audio.volume = volume;
    }

    public void Play()
    {
        audio.Play();
    }

    public void Death()
    {
        StartCoroutine("DeathEffects");
    }

    IEnumerator DeathEffects()
    {
        audio.Play();
        yield return new WaitForSeconds(timeBeforeDeath);
        Destroy(gameObject);
    }
}
