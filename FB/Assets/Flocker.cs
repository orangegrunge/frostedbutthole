﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flocker : MonoBehaviour
{
    public bool isLeader;
    public int minFlockSize;
    public int maxFlockSize;
    public GameObject flocking;
    public GameObject targetPrefab;
    public Steer2D.Arrive arrive;
    public Steer2D.Pursue pursue;
    public Steer2D.Flock flock;

    private void Start()
    {
        int flockSize = Random.Range(minFlockSize, maxFlockSize);
        if (!flocking) flocking = gameObject;

        for (int i = 0; i < flockSize; i++)
        {
            
        }
    }
}
