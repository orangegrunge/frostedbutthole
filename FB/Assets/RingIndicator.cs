﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingIndicator : MonoBehaviour
{
    [Header("Old Physic Settings")]
    LineRenderer lineRenderer;
    public int pointCount;
    public float radius;
    public float startTreshold;
    public float fullCircle;
    public float rotationSpeed;
    float circleCenterAngle;

    [Header("Debug")]
    public float MaxValue;
    public float CurrentValue;

    private InputController InputController;
    private PlayerController player;


    private void Start()
    {
        player = FindObjectOfType<PlayerController>();
        lineRenderer = GetComponent<LineRenderer>();
        InputController = GetComponentInParent<InputController>();
        CurrentValue = MaxValue;
    }

    private void Update()
    {
        circleCenterAngle += rotationSpeed * Time.deltaTime;
        circleCenterAngle = circleCenterAngle % 360;
        MaxValue = InputController.MaxDelay;
        CurrentValue = InputController.CurrentDelay;
        DrawSausige(CurrentValue, MaxValue);
    }

    private void DrawSausige(float currenValue, float maxValue)
    {
        float relativeValue = currenValue / maxValue;
        if (relativeValue > startTreshold)
        {
            lineRenderer.enabled = true;
            lineRenderer.positionCount = Mathf.Max(2, pointCount);
            List<Vector3> points = new List<Vector3>();

            float startAngle = circleCenterAngle - fullCircle * relativeValue / 2;

            for (int i = 0; i < lineRenderer.positionCount; i++)
            {
                float angle = startAngle + i * (fullCircle * relativeValue) / (lineRenderer.positionCount - 1);

                Vector3 segment = new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle)) * radius;
                points.Add((Vector2)transform.position + (Vector2)segment);
            }

            lineRenderer.SetPositions(points.ToArray());
        }
        else
        {
            lineRenderer.enabled = false;
        }
    }
}