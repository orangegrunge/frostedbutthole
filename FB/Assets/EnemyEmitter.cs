﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEmitter : MonoBehaviour
{
    [Header("GD Settings")]
    [Range(0f, 1f)]
    public float spawnSidesPart;
    public EnemyTiersPrefubDict prefubs;
    public EnemyTiersFloatDict tiersMinDepth;
    public EnemyTiersFloatDict tiersMaxDepth;
    public EnemyTiersFloatDict tiersDelayMedian;
    public EnemyTiersFloatDict tiersDelayDispersion;
    public EnemyTiersCurveDict spawnSpeedCurves;
    public EnemyTiersFloatDict speedXDispersion;
    public EnemyTiersFloatDict speedYDispersion;

    [Header("Debug")]
    public float depth;
    public bool Tier1_CD;
    public bool Tier2_CD;
    public bool Tier3_CD;

    private BoxCollider2D SpawnCollider;
    private InputController inputController;
    public TorpedoEmitterController torpedoEmitter;

    void Start()
    {
        SpawnCollider = GetComponent<BoxCollider2D>();
        Tier1_CD = false;
        Tier2_CD = false;
        Tier3_CD = false;
        inputController = FindObjectOfType<InputController>();
        torpedoEmitter = FindObjectOfType<TorpedoEmitterController>();
    }

    private void Update()
    {
        depth = inputController.currentDepth;
        Tier1_CD = CheckCoroutine(EnemyTier.TIER_1, "SpawnTier1", Tier1_CD);
        Tier2_CD = CheckCoroutine(EnemyTier.TIER_2, "SpawnTier2", Tier2_CD);
        Tier3_CD = CheckCoroutine(EnemyTier.TIER_3, "SpawnTier3", Tier3_CD);
    }

    IEnumerator SpawnTier1()
    {
        yield return SpawnEnemy(EnemyTier.TIER_1);
        StartCoroutine("SpawnTier1");
    }

    IEnumerator SpawnTier2()
    {
        yield return SpawnEnemy(EnemyTier.TIER_2);
        StartCoroutine("SpawnTier2");
    }

    IEnumerator SpawnTier3()
    {
        yield return SpawnEnemy(EnemyTier.TIER_3);
        StartCoroutine("SpawnTier3");
    }

    private WaitForSeconds SpawnEnemy(EnemyTier tier)
    {
        //Spawn coordinates
        float spawnX = SpawnCollider.bounds.center.x + GetSpawnX(SpawnCollider.bounds.max.x - SpawnCollider.bounds.min.x);
        float spawnY = Random.Range(SpawnCollider.bounds.min.y, SpawnCollider.bounds.max.y);

        GameObject enemyObj = Instantiate(prefubs[tier], new Vector3(spawnX, spawnY, 0), Quaternion.identity);
        EnemyController enemy = enemyObj.GetComponent<EnemyController>();
        enemy.tier = tier;

        //Direction
        float dir = GetDirectionX(SpawnCollider.bounds.center.x, spawnX, SpawnCollider.bounds.max.x - SpawnCollider.bounds.min.x);
        enemy.SpeedX *= dir;
        if (dir == -1)
        {
            enemyObj.transform.Rotate(180, 0, 180);
        }

        //Additional Speed
        enemy.SpeedX += Random.Range(enemy.SpeedX - speedXDispersion[tier], enemy.SpeedX + speedXDispersion[tier]);
        enemy.SpeedY += Random.Range(enemy.SpeedY - speedYDispersion[tier], enemy.SpeedY + speedYDispersion[tier]);

        //Torpedo target
        TorpedoTargetController torpedoTarget = enemy.GetComponentInChildren<TorpedoTargetController>();
        if (torpedoTarget)
        {
            torpedoEmitter.RegisterTarget(torpedoTarget);
        }

        //CD
        float median = tiersDelayMedian[tier];
        float dispersion = Mathf.Min(Mathf.Abs(tiersDelayDispersion[tier]), tiersDelayMedian[tier]);

        float curveScale = spawnSpeedCurves[tier].Evaluate(depth / (tiersMaxDepth[tier] - tiersMinDepth[tier]));

        median *= curveScale;
        dispersion *= curveScale;

        return new WaitForSeconds(
            Random.Range(
                median - dispersion,
                median + dispersion
                )
            );
    }

    private float GetSpawnX(float width)
    {
        float maxRange = width / 2;
        float minRange = maxRange * (1 - spawnSidesPart);
        float rnd = Random.Range(-1f, 1f);
        float spawnX = Random.Range(minRange, maxRange);
        return (rnd > 0 ? spawnX : spawnX * (-1));
    }

    private float GetDirectionX(float center, float x, float width)
    {
        float direction = x > center ? -1 : 1;
        return direction;
    }

    private bool CheckGeneratingCondision(EnemyTier tier)
    {
        return depth >= tiersMinDepth[tier] && depth <= tiersMaxDepth[tier];
    }

    private bool CheckCoroutine(EnemyTier tier, string methodName, bool processIndicator)
    {
        if (processIndicator != CheckGeneratingCondision(tier))
        {
            if (CheckGeneratingCondision(tier))
            {
                StartCoroutine(methodName);
                return true;
            }
            else
            {
                StopCoroutine(methodName);
                return false;
            }
        }

        return processIndicator;
    }
}
