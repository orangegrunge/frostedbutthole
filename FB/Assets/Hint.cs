﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class Hint : MonoBehaviour, IPointerExitHandler
{
    [Header("Debug")]
    public Module module;
    public Button button;
    public TextMeshProUGUI desc;

    public void OnPointerExit(PointerEventData eventData)
    {

    }

    public void BuyModule()
    {
        
        module.Apply();
        button.interactable = false;
        FindObjectOfType<ModuleManager>().Unpause();
    }
}
