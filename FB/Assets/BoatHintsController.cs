﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatHintsController : MonoBehaviour
{
    public PartGODict hints;



    public void ShowHint(Part part)
    {
        HideAll();
        if (hints.ContainsKey(part)) hints[part].SetActive(true);
    }

    public void HideAll()
    {
        foreach (GameObject obj in hints.Values)
        {
            obj.SetActive(false);
        }
    }

    public void SetDesc(Part part, string desc)
    {
        if (hints.ContainsKey(part)) hints[part].GetComponent<Hint>().desc.text = desc;
    }
}
