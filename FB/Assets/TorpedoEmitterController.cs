﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steer2D;

public class TorpedoEmitterController : MonoBehaviour
{
    [Header("GD Settings")]
    public GameObject TorpedoPrefub;
    public float findTargetRadius;
    [Header("Debug")]
    public TorpedoTargetController target;
    public bool automatic;
    public bool isAutoCD;
    public List<TorpedoTargetController> targets = new List<TorpedoTargetController>();
    
    private PlayerController player;

    private float lastCD;

    private void Start()
    {
        player = GetComponentInParent<PlayerController>();
        target = null;
        automatic = false;
        lastCD = player.stats[ST.AUTOMATION_CD];
    }

    private void Update()
    {
        automatic = player.turretType == TurretType.FISH;

        if (automatic)
        {
            if (!target)
            {
                target = FindTarget();
            }

            if (!isAutoCD && target)
            {
                lastCD = player.stats[ST.AUTOMATION_CD];
                SpawnTorpedo(target);
                StartCoroutine("AutoCD");
            }
        }
        
        if (!automatic || lastCD != player.stats[ST.AUTOMATION_CD])
        {
            StopCoroutine("AutoCD");
            isAutoCD = false;
        }
    }

    public void SpawnTorpedo(TorpedoTargetController target)
    {
        GameObject torpedo = Instantiate(TorpedoPrefub, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
        torpedo.GetComponent<ConstSpeedPursue>().TargetAgent = target.gameObject.GetComponent<SteeringAgent>();
    }

    private TorpedoTargetController FindTarget()
    {
        float maxDist = findTargetRadius;
        TorpedoTargetController resultTarget = null;
        foreach (TorpedoTargetController target in targets)
        {
            if (target.transform.position.y < transform.position.y)
            {
                float dist = Vector3.Distance(transform.position, target.transform.position);
                if (dist < maxDist)
                {
                    maxDist = dist;
                    resultTarget = target;
                }
            }
        }

        return resultTarget;
    }

    public void RegisterTarget(TorpedoTargetController target)
    {
        targets.Add(target);
    }

    public void UnregisterTarget(TorpedoTargetController target)
    {
        targets.Remove(target);
        targets = targets.FindAll(x => x != null);
    }

    IEnumerator AutoCD()
    {
        isAutoCD = true;
        yield return new WaitForSeconds(player.stats[ST.AUTOMATION_CD]);
        isAutoCD = false;
    }
}
