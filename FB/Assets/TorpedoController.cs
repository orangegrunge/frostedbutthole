﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steer2D;

public class TorpedoController : MonoBehaviour
{
    [Header("GD Settings")]
    public float Damage;
    public GameObject alternativeTargetPrefub;
    public AudioClip launch;
    public AudioClip explosion;
    public float timeBeforeDeath;

    private SteeringAgent target;
    private ConstSpeedPursue pursue;
    private KeepMoving keepMoving;
    private Vector3 direction;
    private Bounds DeathBounds;

    private AudioSource audio;
    private SpriteRenderer renderer;

    private SteeringAgent alternativeTarget;

    private void Start()
    {
        pursue = GetComponent<ConstSpeedPursue>();
        target = pursue.TargetAgent;
        DeathBounds = FindObjectOfType<RemoverController>().gameObject.GetComponent<BoxCollider2D>().bounds;
        direction = transform.position;

        alternativeTarget = GenerateAlternativeTarget();
        audio = GetComponent<AudioSource>();
        audio.clip = launch;
        audio.Play();

        renderer = GetComponentInChildren<SpriteRenderer>();
    }

    void Update()
    {
        CheckTarget();
        CheckDeath();
    }

    private void CheckTarget()
    {
        if (!target)
        {
            pursue.TargetAgent = alternativeTarget;
        }
    }

    private void CheckDeath()
    {
        if (
            transform.position.y <= DeathBounds.min.y ||
            transform.position.y >= DeathBounds.max.y ||
            transform.position.x <= DeathBounds.min.x ||
            transform.position.x >= DeathBounds.max.x ||
            Vector3.Distance(transform.position, alternativeTarget.transform.position) < 0.5
            )
        {
            Death();
        }
    }

    private SteeringAgent GenerateAlternativeTarget()
    {
        float dist = Vector3.Distance(transform.position, target.transform.position);
        Vector3 sub = target.gameObject.transform.position - transform.position;
        sub.x *= (20f / dist);
        sub.y *= (20f / dist);
        Vector3 result = target.gameObject.transform.position + sub;

        return Instantiate(
            alternativeTargetPrefub,
            new Vector3(
                result.x,
                result.y,
                0
                ),
            Quaternion.identity
            ).GetComponent<SteeringAgent>();
    }

    public void Death()
    {
        StartCoroutine("DeathEffects");
    }

    IEnumerator DeathEffects()
    {
        if (explosion)
        {
            audio.clip = explosion;
            audio.Play();
        }
        renderer.enabled = false;
        GetComponent<SteeringAgent>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(timeBeforeDeath);
        Destroy(alternativeTarget.gameObject);
        Destroy(gameObject);
    }
}
