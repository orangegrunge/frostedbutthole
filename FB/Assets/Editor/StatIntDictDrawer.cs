﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(StatIntDict))]
public class StatIntDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(PartGODict))]
public class PartGODictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(StringColorDict))]
public class StringColorDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(PartStringDict))]
public class PartStringDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(StatStringDict))]
public class StatStringDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(TriggerStringDict))]
public class TriggerStringDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(TurretTypeStringDict))]
public class TurretTypeStringDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(BubbleTiersFloatDict))]
public class BubbleTiersFloatDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(BubbleTiersCurveDict))]
public class BubbleTiersCurveDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(EnemyTiersFloatDict))]
public class EnemyTiersFloatDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(EnemyTiersCurveDict))]
public class EnemyTiersCurveDictDrawer : SerializableDictionaryPropertyDrawer
{

}

[CustomPropertyDrawer(typeof(EnemyTiersPrefubDict))]
public class EnemyTiersPrefubDictDrawer : SerializableDictionaryPropertyDrawer
{

}