﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BubbleTier
{
    SMALL,
    NORMAL,
    BIG
}

public class BubbleController : ClickableController
{
    [Header("GD Settings")]
    public BubbleTier tier;
    public BubbleTiersFloatDict tiersO2;
    public BubbleTiersFloatDict tiersScale;
    public AudioClip death;
    public float timeBeforeDeath;
    public GameObject soundObject;

    [Header("Physic Settings")]
    public float SpeedY;
    public float SpeedX;
    public float Frequency;

    [Header("Debug")]
    public float O2;
    public SoundController sc;

    private float StartX;
    private float Timer;
    private Bounds DeathBounds;
    private PlayerController player;
    private InputController inputController;
    private BubbleDestroyer bubbleDestroyer;
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        O2 = tiersO2[tier];
        transform.localScale = new Vector3(tiersScale[tier], tiersScale[tier], tiersScale[tier]);

        StartX = transform.position.x;
        Timer = Random.Range(0, 3);
        DeathBounds = FindObjectOfType<RemoverController>().GetComponent<BoxCollider2D>().bounds;
        player = FindObjectOfType<PlayerController>();
        inputController = FindObjectOfType<InputController>();
        bubbleDestroyer = FindObjectOfType<BubbleDestroyer>();
        bubbleDestroyer.RegisterTarget(this);

        audio = GetComponent<AudioSource>();
        audio.clip = death;

        sc = Instantiate(soundObject, new Vector3(0, 0, 0), Quaternion.identity).GetComponent<SoundController>();
        sc.clip = death;
        sc.timeBeforeDeath = timeBeforeDeath;
        sc.volume = audio.volume;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        CheckDeath();
    }

    private void Move()
    {
        float boatSpeed = inputController.speed;
        Timer += Time.deltaTime;
        transform.position = new Vector3(
            transform.position.x + Mathf.Sin(Timer * Frequency) * Time.deltaTime * SpeedX,
            transform.position.y + (SpeedY + boatSpeed ) * Time.deltaTime,
            transform.position.z
            );
    }

    private void GetOxygen()
    {
        Debug.Log("Bubble was destroyed. It droped " + O2 + " Oxygen.");
        player.CallTrigger(Trigger.BUBBLE, O2, transform.position, 0);
        O2 = 0;
    }

    override public bool ClickReaction()
    {
        GetOxygen();
        sc.Death();
        return true;
    }

    private void CheckDeath()
    {
        if (
            transform.position.y <= DeathBounds.min.y ||
            transform.position.y >= DeathBounds.max.y ||
            transform.position.x <= DeathBounds.min.x ||
            transform.position.x >= DeathBounds.max.x ||
            O2 == 0
            )
        {
            bubbleDestroyer.UnregisterTarget(this);
            Destroy(gameObject);
        }
    }
}
