﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelStarter : MonoBehaviour
{
    public bool started;

    void Awake()
    {
        LevelStarter[] objs = FindObjectsOfType<LevelStarter>();

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
