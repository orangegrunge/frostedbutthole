﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    [Header("GD Settings")]
    public float maxDepth;
    public float currentDepth;
    public float speed;

    [Header("Debug")]
    public float MaxDelay;
    public bool IsReady;
    public float CurrentDelay;
    public bool OnTheBottom;

    private PlayerController player;
    public BoatHintsController hintsController;
    public bool clickBlock;
    private bool dead;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        hintsController = FindObjectOfType<BoatHintsController>();
        MaxDelay = player.stats[ST.CLICK_CD];
        IsReady = true;
        CurrentDelay = MaxDelay;
        OnTheBottom = false;
    }

    void Update()
    {
        if (MaxDelay != player.stats[ST.CLICK_CD])
        {
            CurrentDelay = player.stats[ST.CLICK_CD] * CurrentDelay / MaxDelay;
        }
        MaxDelay = player.stats[ST.CLICK_CD];
        CheckBottom();
        CheckClick();
        if (!IsReady)
        {
            CurrentDelay += Time.deltaTime;
            if (CurrentDelay >= MaxDelay) {
                CurrentDelay = MaxDelay;
                IsReady = true;
            }
        }
    }

    void CheckClick()
    {
        if (Input.GetMouseButtonDown(0) && !clickBlock)
        {
            ActionClick();
        }
    }

    private void ActionClick()
    {
        if (IsReady)
        {
            bool clickReaction = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, ray.direction, Mathf.Infinity);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider != null)
                {
                    Debug.Log(hit.collider.gameObject.name);
                    if (hit.collider.gameObject.GetComponentInChildren<ClickableController>())
                    {
                        ClickableController controller = hit.collider.gameObject.GetComponentInChildren<ClickableController>();
                        clickReaction = controller.ClickReaction() || clickReaction;
                    }
                }
            }

            Debug.Log("Click Reaction: " + clickReaction);
            if (clickReaction)
            {
                clickReaction = false;
                IsReady = false;
                CurrentDelay = 0;
            }
        }
        else
        {
            Debug.Log("Click is not ready.");
        }
    }

    internal void Die()
    {
        dead = true;
    }

    IEnumerator StartCD()
    {
        IsReady = false;
        CurrentDelay = 0;
        yield return new WaitForSeconds(MaxDelay);
        IsReady = true;
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        hintsController.HideAll();
    }

    private void CheckBottom()
    {
        if (!OnTheBottom)
        {
            currentDepth += Time.deltaTime * speed;
            if (currentDepth >= maxDepth)
            {
                currentDepth = maxDepth;
                OnTheBottom = true;
                clickBlock = true;
                if (!dead) FindObjectOfType<PlayerController>().body.SetTrigger("win");
            }
        }
        else
        {
            GetBottom();
            clickBlock = true;
        }
    }

    public void GetBottom()
    {
        speed = 0;
    }

    
}
