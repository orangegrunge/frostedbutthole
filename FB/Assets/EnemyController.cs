﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyTier
{
    TIER_1,
    TIER_2,
    TIER_3
}

public class EnemyController : ClickableController
{
    [Header("GD Settings")]
    public Animator MyAnimator;
    public EnemyTier tier;
    public float MaxHP;
    public float Damage;
    public float Metal;
    public AudioClip death;
    public float timeBeforeDeath;
    public GameObject soundObject;

    [Header("Physic Settings")]
    public float SpeedY;
    public float SpeedX;
    public float Frequency;

    [Header("Debug")]
    public float CurrentHP;
    public SoundController sc;

    private float StartX;
    private float Timer;
    private Bounds DeathBounds;

    private PlayerController player;
    private InputController inputController;
    public TorpedoEmitterController torpedoEmitter;
    private AudioSource audio;
    private CircleCollider2D collider;

    static int layer = 1;

    void Start()
    {
        StartX = transform.position.x;
        Timer = Random.Range(0, 3);
        DeathBounds = FindObjectOfType<RemoverController>().gameObject.GetComponent<BoxCollider2D>().bounds;
        player = FindObjectOfType<PlayerController>();
        inputController = FindObjectOfType<InputController>();
        CurrentHP = MaxHP;
        torpedoEmitter = FindObjectOfType<TorpedoEmitterController>();

        collider = GetComponent<CircleCollider2D>();

        audio = GetComponent<AudioSource>();
        audio.clip = death;

        sc = Instantiate(soundObject, new Vector3(0, 0, 0), Quaternion.identity).GetComponent<SoundController>();
        sc.clip = death;
        sc.timeBeforeDeath = timeBeforeDeath;
        sc.volume = audio.volume;

        var allSprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var s in allSprites)
        {
            s.sortingLayerName = layer.ToString();
            layer++;
            if (layer == 10) layer = 1;
        }
    }

    void Update()
    {
        Move();
        CheckDeath();
    }

    private void Move()
    {
        float boatSpeed = inputController.speed;
        Timer += Time.deltaTime;
        transform.position = new Vector3(
            transform.position.x + SpeedX * Time.deltaTime,
            transform.position.y + (SpeedY + boatSpeed) * Time.deltaTime,
            transform.position.z
            );

        Vector3 newDirection = Vector3.RotateTowards(transform.forward, transform.forward, 1000000f, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    public void GetMetal()
    {
        Debug.Log("Enemy was destroyed. It droped " + Metal + " Metal.");
        player.CallTrigger(Trigger.ENEMY, Metal, transform.position, 0);
    }

    public void GetMetalHit()
    {
        Debug.Log("Enemy was hit.");
        player.CallTrigger(Trigger.ENEMY, 0, transform.position, 0);
    }

    private void GetDamage()
    {
        Debug.Log("Enemy attacked. Input damage: " + Damage);
        player.CallTrigger(Trigger.DAMAGE, Damage, transform.position, Metal);
    }

    override public bool ClickReaction()
    {
        return false;
    }

    private void CheckDeath()
    {
        if (
            transform.position.y <= DeathBounds.min.y ||
            transform.position.y >= DeathBounds.max.y ||
            transform.position.x <= DeathBounds.min.x ||
            transform.position.x >= DeathBounds.max.x ||
            CurrentHP == 0
            )
        {

            torpedoEmitter.UnregisterTarget(GetComponentInChildren<TorpedoTargetController>());

            if (!MyAnimator) MyAnimator = GetComponent<Animator>();
            MyAnimator.SetBool("Destroyed", true);
            collider.enabled = false;
            GetComponentInChildren<TorpedoTargetController>().GetComponent<Collider2D>().enabled = false;
            Destroy(GetComponentInChildren<Steer2D.SteeringAgent>());
            //Destroy(gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponentInParent<PlayerController>())
        {
            //GetMetal();
            GetDamage();
            torpedoEmitter.UnregisterTarget(GetComponentInChildren<TorpedoTargetController>());
            sc.Death();
            GetComponentInChildren<TorpedoTargetController>().GetComponent<Collider2D>().enabled = false;
            Destroy(GetComponentInChildren<Steer2D.SteeringAgent>());
            //Destroy(gameObject);
            collider.enabled = false;
            MyAnimator.SetBool("Destroyed", true);
        }
    }
    public void Destroy()
    {
        Destroy(gameObject);
    }
}