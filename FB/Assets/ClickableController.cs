﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClickableController : MonoBehaviour
{
    public abstract bool ClickReaction();
}
