﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorpedoTargetController : ClickableController
{
    [Header("Debug")]
    public TorpedoEmitterController torpedoEmitter;

    private void Start()
    {
        torpedoEmitter = FindObjectOfType<TorpedoEmitterController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        TorpedoController torpedo = other.gameObject.GetComponent<TorpedoController>();
        if (torpedo)
        {
            EnemyController enemy = transform.parent.gameObject.GetComponent<EnemyController>();
            if (enemy)
            {
                enemy.CurrentHP = Mathf.Max(enemy.CurrentHP - torpedo.Damage, 0);
                if (enemy.CurrentHP == 0)
                {
                    torpedo.Death();
                    enemy.GetMetal();
                    enemy.sc.Death();
                    //torpedoEmitter.UnregisterTarget(this);
                    //Destroy(enemy.gameObject);
                }
                else
                {
                    enemy.GetMetalHit();
                    torpedo.Death();
                    enemy.sc.Play();
                }
            }
        }
    }

    override public bool ClickReaction()
    {
        FindObjectOfType<TorpedoEmitterController>().SpawnTorpedo(this);
        Debug.Log("Torped was Spawned");
        return true;
    }
}
