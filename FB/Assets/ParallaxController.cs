﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParallaxLayer
{
    public List<GameObject> objs;
    public SortingLayer layer;
    public float speed;

}

public class ParallaxController : MonoBehaviour
{
    public List<ParallaxLayer> layers;

    private void Update()
    {
        foreach (ParallaxLayer layer in layers)
        {
            foreach (GameObject obj in layer.objs)
            {
                obj.transform.Translate(Vector2.up * layer.speed * Time.deltaTime);
            }

            if (layer.objs[0].transform.position.y > layer.objs[0].GetComponent<SpriteRenderer>().bounds.size.y) {
                //remove;
                Destroy(layer.objs[0]);
                layer.objs = layer.objs.GetRange(1, layer.objs.Count - 1);
            }

            if (layer.objs[layer.objs.Count - 1].transform.position.y > 0)
            {
                //add;
                GameObject oldObj = layer.objs[layer.objs.Count - 1];
                GameObject newLayerObj = Instantiate(oldObj, 
                    oldObj.transform.parent);
                newLayerObj.name = "l";
                newLayerObj.transform.position = oldObj.transform.position + Vector3.down * oldObj.GetComponent<SpriteRenderer>().bounds.size.y;
                layer.objs.Add(newLayerObj);
            }
        }

        
    }

}
